# Emocon physio utils

I used these scripts when working with electrophysiology data from emotional contagion project.

Includes a copy of cvxEDA code from https://github.com/lciti/cvxEDA
The cvxEDA is distributed under GPL license
Copyright (C) 2014-2015 Luca Citi, Alberto Greco