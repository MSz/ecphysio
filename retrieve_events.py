"""
Functions here are used to retrieve events from "live observation" logfiles
to be reused with EDA recorded during corresponding "video observaton",
where only video start was marked in EDA signal.
"""

import pandas
import re
import os
import glob


def read_logfile(log_fname, fs, shift_by=None):
    rename_dict = {
        'Event Type': 'EventType',
        'Time': 'onset',
        'Code': 'trial_type',
        }

    df = pandas.read_csv(log_fname, sep='\t', skiprows=3)
    df.rename(columns=rename_dict, inplace=True)

    # convert onset to seconds
    df.onset = df.onset / 10000

    # shift onsets if needed (for videos starting after the scenario)
    if shift_by is not None:
        df.onset = df.onset - shift_by

    # rename fix_2 (used in DE for reasons unknown) to fix
    df.trial_type.replace('fix_2', 'fix', inplace=True)

    # Choose relevant rows & columns
    subset = (
        df
        .query('trial_type in ["fix", "cs_plus", "cs_minus", "US_stim_ON"]')
        .loc[:, ['onset', 'trial_type']]
        )

    # Rename US trial_type
    subset.trial_type.replace('US_stim_ON', 'us', inplace=True)

    # Add column with samples
    # sampleNo because series.sample is a method
    subset['sampleNo'] = (subset.onset * fs).round().astype(int)

    # Add 'value' column with corresponding port codes
    subset.loc[:, 'value'] = subset.trial_type.map(
        {'cs_plus': 1, 'cs_minus': 2, 'fix': 7, 'us': 8}
    )

    return subset.reset_index(drop=True)


def find_demonstrator(log_fname):

    substitutions = {
        'GYF_full': 'GYFXOU',
        'GYF_inverse': 'GYFXOU',
        'SUB-1': 'FRAMWY',
        'SUB-2': 'PAZHLQ',
        'QETQJZ': 'QETWJZ'
    }

    with open(log_fname) as f:
        while f.readline().rstrip() != 'video summary':
            pass
        f.readline()
        v_summary = f.readline().rstrip().split('\t')

    # get filename without extension; stimuli\(anything but dot).avi
    dem_code = re.search(r'stimuli\\([^.]+)\.avi', v_summary[0]).group(1)
    if dem_code in substitutions:
        dem_code = substitutions[dem_code]
    return dem_code


def get_events_table(log_dir, code, fs, shift_by=None):
    vid_log = glob.glob(os.path.join(log_dir, code + '-ofl_v_[12].log'))[0]
    dem_code = find_demonstrator(vid_log)
    src_log = os.path.join(log_dir, dem_code + '-procedure OFL.log')
    print('Retrieving events from source: ', src_log)
    table = read_logfile(src_log, fs, shift_by)
    return(table)
